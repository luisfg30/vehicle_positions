class AggregatedEvent():

    # On construction this class uses data from a vehicle event
    def __init__(self, vehicle_event):
        self.vehicle_id = vehicle_event["vehicle_id"]
        self.timestamp = vehicle_event["timestamp"]
        self.is_stationary = vehicle_event["is_stationary"]
        self.latitude = vehicle_event["latitude"]
        self.longitude = vehicle_event["longitude"]
        # Array of tuples sorted by POI name
        self.is_inside_poi = vehicle_event["is_inside_poi"]
        # All times are stored in seconds
        self.total_time_stationary = 0
        self.total_time_inside_poi = self.init_inside_poi_structure(
            self.is_inside_poi)
        self.total_time_stationary_poi = self.init_inside_poi_structure(
            self.is_inside_poi)

    def init_inside_poi_structure(self, boolean_tuple_array):
        time_tuple_array = []
        for t in boolean_tuple_array:
            new_t = (t[0], 0)
            time_tuple_array.append(new_t)
        return dict(time_tuple_array)

    # Aggregation of two consecutive vehicle_events
    def aggregate(self, vehicle_event):

        if self.vehicle_id != vehicle_event["vehicle_id"]:
            return

        # Aggregate time counters, self is always in the past
        time_diff = calculate_datediff_seconds(self.timestamp,
                                               vehicle_event["timestamp"])
        # Only count two consecutive stationary events
        if self.is_stationary and vehicle_event["is_stationary"]:
            self.total_time_stationary += time_diff

        self.update_total_time_poi(vehicle_event, time_diff)

        # Update position and timestamp
        self.latitude = vehicle_event["latitude"]
        self.longitude = vehicle_event["longitude"]
        self.timestamp = vehicle_event["timestamp"]

    def update_total_time_poi(self, vehicle_event, time_diff):
        for t in list(zip(self.is_inside_poi, vehicle_event["is_inside_poi"])):
            is_inside_aggregated = t[0][1]
            is_inside_event = t[1][1]
            if is_inside_aggregated and is_inside_event:
                poi_name = t[0][0]
                self.total_time_inside_poi[poi_name] += time_diff
                if self.is_stationary and vehicle_event["is_stationary"]:
                    self.total_time_stationary_poi[poi_name] += time_diff

    def convert_to_flat_dict(self, poi_names):
        time_inside_names = list(map(lambda name: "ti_" + name, poi_names))
        time_stationary_names = list(map(lambda name: "ts_" + name, poi_names))
        row = {}
        row["vehicle_id"] = self.vehicle_id
        row["timestamp"] = self.timestamp
        row["latitude"] = self.latitude
        row["longitude"] = self.longitude
        row["total_time_stationary"] = self.total_time_stationary
        row.update(
            dict(zip(time_inside_names, self.total_time_inside_poi.values())))
        row.update(
            dict(zip(time_stationary_names, self.total_time_stationary_poi.values())))
        return row


def calculate_datediff_seconds(datetime1, datetime2):
    return (datetime2 - datetime1).total_seconds()
