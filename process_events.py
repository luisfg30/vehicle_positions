#!/usr/bin/env python3

from aggregatedEvent import AggregatedEvent
from datetime import datetime
from haversine import haversine_vector, Unit

import argparse
import numpy as np
import pandas as pd


def main(*args, **kwargs):
    # Read data from csv, calculate new columns and filter events
    df = load_data(kwargs.get('events_path'))
    df["timestamp"] = df.apply(
        lambda row: parse_datetime(row.data_posicao), axis=1)
    df["is_stationary"] = df.apply(
        lambda row: row.velocidade < 5 and not row.ignicao, axis=1)
    df = df.rename(columns={"placa": "vehicle_id"})
    df = df.drop(["data_posicao", "velocidade", "ignicao"], axis=1)

    # Filter by vehicle_id
    vids = df["vehicle_id"].unique()

    vehicle_id_goups = []
    for vid in vids:
        filtered_df = df.loc[df['vehicle_id'] == vid]
        vehicle_id_goups.append(filtered_df.to_dict(orient='records'))

    # Compute aggregated events by vehicle id groups
    df_poi = load_data(kwargs.get('poi_path'))
    aggregated_events = []
    for group in vehicle_id_goups:
        aggregated_events.append(process_events(group, df_poi))

    # Compute fleet inside and stationary total time by POI
    poi_names = df_poi["nome"].tolist()
    poi_names.sort()
    flat_dicts = []
    index_labels = []
    for event in aggregated_events:
        row = event.convert_to_flat_dict(poi_names)
        flat_dicts.append(row)
        index_labels.append(row["vehicle_id"])

    consolidated_df = pd.DataFrame.from_records(flat_dicts, index=index_labels)
    consolidated_df.loc["Fleet_Total"] = consolidated_df.sum(
        axis=0, numeric_only=True)

    consolidated_df.to_csv(kwargs.get('output_path'))


def process_events(vehicle_events, df_poi):
    first_event = vehicle_events[0]
    is_inside_poi = compute_boundaries(
        first_event["latitude"], first_event["longitude"], df_poi)
    first_event["is_inside_poi"] = is_inside_poi
    aggregated_event = AggregatedEvent(first_event)
    vehicle_events.pop(0)

    for event in vehicle_events:
        event["is_inside_poi"] = is_inside_poi
        aggregated_event.aggregate(event)

    return aggregated_event

# Auxiliary functions


def load_data(filepath):
    return pd.read_csv(filepath)


def parse_datetime(string):
    return datetime.strptime(string, "%a %b %d %Y %H:%M:%S %Z%z (Hora oficial do Brasil)")


def extract_columns_as_array(df, columns):
    positions = df[columns]
    return positions.to_records(index=False).tolist()


def compute_distance_from_poi(vehicle_position, points):
    vehicle_array = np.full((len(points), 2), vehicle_position)
    return haversine_vector(vehicle_array, points, Unit.METERS)


def compute_boundaries(latitude, longitude, df_poi):
    positions = extract_columns_as_array(df_poi, ["latitude", "longitude"])
    vehicle_position = (latitude, longitude)
    distances = compute_distance_from_poi(vehicle_position, positions)
    df = pd.DataFrame(distances, columns=["distancia"])
    df_poi = df_poi.join(df)
    df_poi["is_inside"] = df_poi.apply(
        lambda row: row.distancia <= row.raio, axis=1)
    is_inside_poi = df_poi[["nome", "is_inside"]
                           ].to_records(index=False).tolist()
    is_inside_poi.sort(key=lambda tup: tup[0])
    return is_inside_poi


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Batch aggregation of vehicle events")
    parser.add_argument('-e', '--events', dest='events_path',
                        help='Vehicle events CSV file', action='store', metavar='events_path', required=True)
    parser.add_argument('-p', '--poi', dest='poi_path',
                        help='Points of Interest CSV file', action='store', metavar='poi_path', required=True)
    parser.add_argument('-o', '--output', dest='output_path',
                        help='Output path for consolidated events in CSV format', action='store', metavar='output_path',
                        required=False, default="resultados_consolidado_POIs.csv")
    args = parser.parse_args()
    main(**vars(args))
