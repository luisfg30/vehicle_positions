## Requirements
Python 3.8.x

## Installation details
`pip3 install --upgrade -r requirements.txt`


## Run example - batch processing
`process_events.py -e csv_data/posicoes.csv -p csv_data/base_pois_def.csv`

The default output file for the result is "resultados_consolidado_POIs.csv"

## Check the wiki for more details
[Wikis home](https://gitlab.com/luisfg30/vehicle_positions/-/wikis/home)
